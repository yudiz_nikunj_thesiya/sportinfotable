import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import Helmet from "react-helmet";
import HeroSection from "./components/HeroSection";
import Nav from "./components/Nav";

function App() {
	const [data, setData] = useState([]);
	useEffect(() => {
		fetchData();
	}, []);
	async function fetchData() {
		axios
			.get(
				"https://backend.sports.info/api/v1/posts/recent",
				{
					headers: {
						"Content-Type": "application/json",
					},
				},
				{
					nStart: 0,
					nLimit: 8,
					eSort: "Latest",
					bRemoveBannerPosts: true,
				}
			)
			.then((response) => {
				setData(response.data.data);
				console.log(response.data.data);
			});
	}

	return (
		<div className="App">
			<Helmet>
				<meta charSet="utf-8" />
				<title>Home</title>
			</Helmet>
			<Nav />
			<HeroSection tableData={data} />
		</div>
	);
}

export default App;

import React from "react";
import { format } from "date-fns";
export const COLUMNS = [
	{
		Header: "Image",
		accessor: "sImage",
		Cell: (tableProps) => (
			<img src={tableProps.row.original.sImage} width={60} alt="sport" />
		),
		Footer: "Image",
	},
	{
		Header: "Title",
		accessor: "sTitle",
		Footer: "Title",
	},
	{
		Header: "Description",
		accessor: "sDescription",
		Footer: "Description",
	},
	{
		Header: "Type",
		accessor: "eType",
		Footer: "Type",
	},
	{
		Header: "Created At",
		accessor: "dCreatedAt",
		Cell: ({ value }) => {
			return format(new Date(value), "dd/MM/yyyy");
		},
		Footer: "Created At",
	},
	{
		Header: "View Counts",
		accessor: "nViewCounts",
		Footer: "View Counts",
	},
	{
		Header: "Comments Count",
		accessor: "nCommentsCount",
		Footer: "Comments Count",
	},
];

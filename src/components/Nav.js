import React from "react";

const Nav = () => {
	return (
		<div className="bg-purple-900 w-full box-border flex items-center justify-between px-10 py-6">
			<div className="text-purple-100 text-2xl font-bold">SportsInfo</div>
			<div className="md:flex hidden items-center space-x-3 text-purple-300">
				<span className="active">Home</span>
			</div>
		</div>
	);
};

export default Nav;

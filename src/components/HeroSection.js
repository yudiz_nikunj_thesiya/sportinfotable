import React, { useMemo, useState, useEffect } from "react";
import { usePagination, useSortBy, useTable } from "react-table";
import { COLUMNS } from "../data/columns";

import { AiFillFastBackward, AiFillFastForward } from "react-icons/ai";
import {
	BsArrowLeftShort,
	BsArrowRightShort,
	BsFillArrowDownCircleFill,
	BsFillArrowUpCircleFill,
} from "react-icons/bs";

const HeroSection = ({ tableData }) => {
	const columns = useMemo(() => COLUMNS, []);
	const data = useMemo(() => tableData, [tableData]);

	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		footerGroups,
		page,
		nextPage,
		previousPage,
		canNextPage,
		canPreviousPage,
		prepareRow,
		pageOptions,
		state,
		setPageSize,
		gotoPage,
		pageCount,
	} = useTable(
		{
			columns,
			data,
			initialState: { pageSize: 4 },
		},
		useSortBy,
		usePagination
	);
	const { pageIndex, pageSize } = state;

	return (
		<>
			<div className="bg-purple-900 w-full box-border flex items-start justify-start px-10"></div>
			<div className="bg-purple-900 w-full box-border px-10 py-10 flex min-h-[80vh] justify-center">
				<div className="overflow-x-auto">
					<table {...getTableProps()} className="table-auto rounded-xl">
						<thead>
							{headerGroups.map((headerGroup) => (
								<tr {...headerGroup.getHeaderGroupProps()}>
									{headerGroup.headers.map((column) => (
										<th className="whitespace-nowrap">
											<span
												{...column.getHeaderProps(
													column.getSortByToggleProps()
												)}
											>
												{column.render("Header")}
											</span>
											<span className="flex whitespace-nowrap items-center justify-center">
												{column.isSorted ? (
													column.isSortedDesc ? (
														<BsFillArrowDownCircleFill />
													) : (
														<BsFillArrowUpCircleFill />
													)
												) : (
													""
												)}
											</span>
											<span className="">
												{column.canFilter ? column.render("Filter") : null}
											</span>
										</th>
									))}
								</tr>
							))}
						</thead>
						<tbody {...getTableBodyProps()}>
							{page.map((row, i) => {
								prepareRow(row);
								return (
									<tr key={i} {...row.getRowProps()}>
										{row.cells.map((cell, i) => {
											return (
												<td {...cell.getCellProps()} key={i}>
													{cell.render("Cell")}
												</td>
											);
										})}
									</tr>
								);
							})}
						</tbody>

						<tfoot>
							{footerGroups.map((footerGroup) => (
								<tr {...footerGroup.getHeaderGroupProps()}>
									{footerGroup.headers.map((column) => (
										<td {...column.getFooterProps}>
											{column.render("Footer")}
										</td>
									))}
								</tr>
							))}
						</tfoot>
					</table>
				</div>
			</div>
			<div className="bg-purple-900 w-full box-border flex flex-col md:flex-row items-center justify-between px-10 pb-10 space-y-6 md:space-y-0 md:space-x-4">
				<div className="text-purple-400 flex items-center space-x-2">
					<span>Page</span>
					<input
						type="number"
						defaultValue={pageIndex + 1}
						value={pageIndex + 1}
						onChange={(e) => {
							const pageNumber = e.target.value
								? Number(e.target.value) - 1
								: 0;
							gotoPage(pageNumber);
						}}
						className="w-20 px-5 py-2.5 rounded-xl bg-purple-200 text-purple-900 font-semibold outline-none"
					/>

					<span>of</span>
					<span className="px-5 py-2.5 rounded-xl bg-purple-200 text-purple-900 font-semibold">
						{pageOptions.length}
					</span>

					<select
						value={pageSize}
						onChange={(e) => setPageSize(e.target.value)}
						className="px-5 py-2.5 rounded-xl bg-purple-200 text-purple-900 font-normal outline-none"
					>
						{[2, 4, 6, 8].map((pageSize) => (
							<option key={pageSize} value={pageSize}>
								Show {pageSize}
							</option>
						))}
					</select>
				</div>

				<div className="flex items-center space-x-4">
					<button
						className={
							canPreviousPage === false
								? "disabled py-4 flex items-center space-x-2"
								: "btn py-4 flex items-center space-x-2"
						}
						onClick={() => gotoPage(0)}
						disabled={!canPreviousPage}
					>
						<AiFillFastBackward className="text-lg" />
					</button>

					<button
						className={
							canPreviousPage === false
								? "disabled flex items-center space-x-2"
								: "btn flex items-center space-x-2"
						}
						onClick={() => previousPage()}
						disabled={!canPreviousPage}
					>
						<BsArrowLeftShort className="text-lg" />
						<span>Previous</span>
					</button>
					<button
						className={
							canNextPage === false
								? "disabled flex items-center space-x-2"
								: "btn flex items-center space-x-2"
						}
						onClick={() => nextPage()}
						disabled={!canNextPage}
					>
						<span>Next</span>
						<BsArrowRightShort />
					</button>
					<button
						className={
							canNextPage === false
								? "disabled py-4 flex items-center space-x-2"
								: "btn py-4 flex items-center space-x-2"
						}
						onClick={() => gotoPage(pageCount - 1)}
						disabled={!canNextPage}
					>
						<AiFillFastForward className="text-lg" />
					</button>
				</div>
			</div>
		</>
	);
};

export default HeroSection;
